# Bright Solutions - Phing Tasks

Phing: https://www.phing.info/

The repository contains the Bright Solutions Drupal Phing tasks.
It can be added to a project by using composer.

Repository section:

```JSON
{
    "type": "vcs",
    "url":  "git@git.brightsolutions.de:drupal/bs-phing-tasks.git"
}
```

Require package section:

```JSON
{
    "brightsolutions/phing-tasks": "1.x-dev"
}
```

Afterwards just run `composer install`.

Further explanation about the Phing tasks could be found in the Bright Solutions Drupal Template README.md file: https://git.brightsolutions.de/drupal/project-template/blob/d8/README.md
